package repository;

import model.Cliente;

import java.util.List;

public interface ClienteRepository {

    List<Cliente> obterTodos();

    Cliente obterPorId(final Long id);

    void atualizar(final Cliente Cliente);

    void Salvar(final Cliente Cliente);

    void excluir(final Long id);

}
