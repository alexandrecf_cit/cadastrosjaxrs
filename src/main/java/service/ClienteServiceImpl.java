package service;

import model.Cliente;
import repository.ClienteRepository;
import repository.UsuarioRepository;
import resource.ClienteResource;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Named
@RequestScoped
public class ClienteServiceImpl implements ClienteService {

    private static Logger log = Logger.getLogger(ClienteResource.class.getName());

    @Inject
    private ClienteRepository clienteRepository;

    @Inject
    private UsuarioRepository usuarioRepository;

    @Override
    public List<Cliente> obterTodos() {
        return clienteRepository.obterTodos();
    }

    @Override
    public Cliente obterPorId(Long id) {
        return  clienteRepository.obterPorId(id);
    }

    @Override
    public Response atualizar(Cliente cliente) {

        Cliente clienteAnterior = clienteRepository.obterPorId(cliente.getId());
        if(clienteAnterior == null)
            return Response.status(Response.Status.NOT_FOUND).build();

        try{
            cliente.setId(clienteAnterior.getId());
            clienteRepository.atualizar(cliente);
            return Response.status(Response.Status.OK).entity(cliente).build();
        }
        catch(Exception ex)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }

    }

    @Override
    public Response Salvar(Cliente cliente) {

        try{
            clienteRepository.Salvar(cliente);
            return Response.status(Response.Status.CREATED).entity(cliente).build();
        }
        catch(Exception ex)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }

    }

    @Override
    public Response excluir(Long id, String usuario, String senha) {

        if (!usuarioRepository.validarUsuario(usuario,senha)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Cliente clienteExistente = clienteRepository.obterPorId(id);
        if(clienteExistente == null)
            return Response.status(Response.Status.NOT_FOUND).build();

        try{
            clienteRepository.excluir(id);
            return Response.status(Response.Status.OK).build();
        }
        catch(Exception ex)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
}
