package resource;

import model.Cliente;
import model.RequisicaoEnum;
import service.ClienteService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("clientes")
public class ClienteResource {

    private static Logger LOGGER = Logger.getLogger(ClienteResource.class.getName());
    private static final String MENSAGEM_REQUISICAO = "Requisicao recebida: ";

    @Inject
    private ClienteService clienteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Cliente> obterTodos() {

        return clienteService.obterTodos();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente obterPorId(@PathParam("id") Long id) {
        LOGGER.info(MENSAGEM_REQUISICAO + RequisicaoEnum.GET.getRequisicao());
        return clienteService.obterPorId(id);
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response salvar(@Valid Cliente cliente) {
        LOGGER.info(MENSAGEM_REQUISICAO + RequisicaoEnum.POST.getRequisicao());
        return clienteService.Salvar(cliente);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response atualizar(@Valid Cliente cliente) {
        LOGGER.info(MENSAGEM_REQUISICAO + RequisicaoEnum.PUT.getRequisicao());
        return clienteService.atualizar(cliente);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response excluir(@NotNull(message = "ID � obrigat�rio!") @QueryParam("id") Long id,
                            @HeaderParam("usuario") String usuario,
                            @HeaderParam("senha") String senha) {
        LOGGER.info(MENSAGEM_REQUISICAO + RequisicaoEnum.DELETE.getRequisicao());
        return clienteService.excluir(id, usuario, senha);
    }

}
