package config;

import model.Cliente;
import repository.ClienteRepository;


import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class RestConfiguration extends Application {

    @Inject
    ClienteRepository clienteRepository;


    @PostConstruct
    public void setUp() {

        Cliente cliente = new Cliente();

        cliente.setId(1L);
        cliente.setNome("Alexandre");
        cliente.setCpfCnpj("306.315.060-63");
        cliente.setTipo("Fisica");

        clienteRepository.Salvar(cliente);

    }

}
