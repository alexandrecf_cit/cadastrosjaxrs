package repository;

import model.Cliente;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Named
@ApplicationScoped
public class ClienteRepositoryImpl implements ClienteRepository {

    private final static HashMap<Long, Cliente> clientes = new HashMap<>();


    @Override
    public List<Cliente> obterTodos() {
        return new ArrayList<Cliente>(clientes.values());
    }

    @Override
    public Cliente obterPorId(Long id) {
        return clientes.get(id);
    }

    @Override
    public void atualizar(Cliente cliente) {
        clientes.remove(cliente.getId());
        clientes.put(cliente.getId(), cliente);
    }

    @Override
    public void Salvar(Cliente cliente) {
        if(cliente.getId() == 0 )
            cliente.setId(gerarId((long) clientes.size() + 1));
        clientes.put(cliente.getId(), cliente);
    }

    @Override
    public void excluir(Long id) {
        clientes.remove(id);
    }

    private Long gerarId(final Long possible)
    {
        if(clientes.containsKey(possible))
            return gerarId(possible + 1);
        return possible;
    }

}
