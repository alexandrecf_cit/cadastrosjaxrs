package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cliente {

    @NotNull
    @Min(value = 0 , message = "Valor  zerado ou negativo")
    @Digits(fraction = 0, integer = 10)
    private Long id;
    @NotNull(message = "O c�digo � obrigat�rio")
    private String nome;
    @NotNull
    private String tipo;
    @NotNull
    private String cpfCnpj;
}
