package service;

import model.Cliente;

import javax.ws.rs.core.Response;
import java.util.List;

public interface ClienteService {

    List<Cliente> obterTodos();

    Cliente obterPorId(final Long id);

    Response atualizar(final Cliente Cliente);

    Response Salvar(final Cliente Cliente);

    Response excluir(final Long id, String usuario, String senha);


}
