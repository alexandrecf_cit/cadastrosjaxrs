package model;

public enum RequisicaoEnum {

    GET("GET"),
    POST("POST"),
    PUT("PUT"),
    DELETE("DELETE");
    private String requisicao;

    private RequisicaoEnum(String requisicao) {
        this.requisicao = requisicao;
    }

    public String getRequisicao() {
        return requisicao;
    }

}
